<?php namespace Blogator\Components\NoSchemaPermission;

use Blogator\Components\NoSchemaPermission\Contracts\PermissionGroupContract;
use Blogator\Components\NoSchemaPermission\Traits\GroupTrait;
use Illuminate\Database\Eloquent\Model;

class Group extends Model implements PermissionGroupContract
{
    use GroupTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'permission_groups';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

}