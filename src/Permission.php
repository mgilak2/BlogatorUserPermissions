<?php namespace Blogator\Components\NoSchemaPermission;
use Blogator\Components\NoSchemaPermission\Traits\UtilsTrait;

/**
 * Class Permission
 * value object - immutable
 * @package Blogator\Components\NoSchemaPermission
 */
class Permission
{
    use UtilsTrait;
    /**
     * @var string
     */
    protected $permission_foreign_id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer could be -1 , 0 , 1
     */
    private $status;

    const OK = "ok";
    const BANED = "baned";
    const noACCESS = "noAccess";

    public function __construct($name, $status = 1)
    {
        $this->name = $name;
        $this->status = $status;
        $this->permission_foreign_id = $this->randomString();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getParsedStatus()
    {
        $status = self::OK;

        if ($this->status == -1) {
            $status = self::BANED;
        } elseif ($this->status == 0) {
            $status = self::noACCESS;
        }

        return $status;
    }

    public function getPermissionForeignId()
    {
        return $this->permission_foreign_id;
    }

    public function simplifyPermission()
    {
        return ['name' => $this->name, 'status' => $this->status];
    }
}