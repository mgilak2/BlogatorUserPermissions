<?php namespace Blogator\Components\NoSchemaPermission;

use Blogator\Components\NoSchemaPermission\Contracts\PermissionGroupContract;
use Blogator\Components\NoSchemaPermission\Contracts\UserPermissionContract;
use Blogator\Components\NoSchemaPermission\Traits\UtilsTrait;

/**
 * Class PermissionGroup
 * @package Blogator\Components\NoSchemaPermission
 */
class PermissionGroup
{
    use UtilsTrait;
    /**
     * @var string
     */
    public $id;

    protected $is_baned = false;

    /**
     * @var string
     */
    public $name;

    /**
     * collection of Blogator\Components\NoSchemaPermission\Permission
     * @var array
     */
    protected $permissions = [];

    /**
     * collection of Blogator\Components\NoSchemaPermission\PermissionGroup
     * @var array
     */
    protected $groups = [];

    public function __construct()
    {
        $this->id = $this->randomString();
    }

    /**
     * @param Permission $permission
     */
    public function add(Permission $permission)
    {
        $this->permissions[$permission->getPermissionForeignId()] = $permission;
    }

    /**
     * @param Permission $permission
     */
    public function remove(Permission $permission)
    {
        if (isset($this->permissions[$permission->getPermissionForeignId()]))
            unset($this->permissions[$permission->getPermissionForeignId()]);
    }

    /**
     * @param PermissionGroup $group
     */
    public function addGroup(PermissionGroup $group)
    {
        $this->groups[$group->getId()] = $group;
    }

    /**
     * @param PermissionGroup $group
     */
    public function removeGroup(PermissionGroup $group)
    {
        if (isset($this->groups[$group->getId()]))
            unset($this->groups[$group->getId()]);
    }

    public function has(Permission $permission)
    {
        if ($this->findInPermissions($permission) && !$this->is_baned) return true;
        if ($this->findPermissionInGroups($permission) && !$this->is_baned) return true;
        return false;
    }

    public function findInPermissions(Permission $permission)
    {
        $this->is_baned = false;
        $simplified = $permission->simplifyPermission();
        foreach ($this->permissions as $stored_permission) {
            $stored_permission_simplified  = $stored_permission->simplifyPermission();
            if ($simplified['name'] == $stored_permission_simplified['name']) {
                $this->is_baned = true;
                return true;
            }
        }

        return false;
    }

    protected function findPermissionInGroups(Permission $permission)
    {
        // group : Blogator\Components\NoSchemaPermission\PermissionGroups
        foreach ($this->groups as $group) {
            if ($group->findInPermissions($permission)) {
                return true;
            }
        }
        return false;
    }

    public function getAllPermissions()
    {
        return $this->permissions;
    }

    public function getAllGroups()
    {
        return $this->groups;
    }

    public function intoJson()
    {
        $std = new \stdClass();
        $std->permissions = [];
        foreach ($this->permissions as $permission) {
            $permissionStd = new \stdClass();
            $permissionStd->name = $permission->getName();
            $permissionStd->status = $permission->getStatus();
            $std->permissions[] = $permissionStd;
        }
        $std->groups = [];
        foreach ($this->groups as $group) {
            $groupStd = new \stdClass();
            $groupStd->name = $group->name;
            $groupStd->id = $group->id;
            $std->groups[] = $groupStd;
        }

        return json_encode($std);
    }

    public function saveUser(UserPermissionContract $model)
    {
        $model->permissions = $this->intoJson();
        $model->save();
    }

    public function saveGroup(PermissionGroupContract $model)
    {
        $model->permissions = $this->intoJson();
        $model->save();
    }

    public function assignUser(UserPermissionContract $model)
    {
        $model->permissions = $this->intoJson();
    }

    public function assignGroup(PermissionGroupContract $model)
    {
        $model->permissions = $this->intoJson();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
}