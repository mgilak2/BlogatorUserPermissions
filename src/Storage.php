<?php namespace Blogator\Components\NoSchemaPermission;
use Blogator\Components\NoSchemaPermission\Contracts\PermissionStorageContract;
use Blogator\Components\NoSchemaPermission\Traits\StorageTrait;

/**
 * Class Storage
 * @package Blogator\Components\NoSchemaPermission
 */
class Storage implements PermissionStorageContract
{
    // tailored for eloquent , that`s why i extracted into a trait
    use StorageTrait;
}