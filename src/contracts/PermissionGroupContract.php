<?php namespace Blogator\Components\NoSchemaPermission\Contracts;

use Blogator\Components\NoSchemaPermission\Permission;
use Blogator\Components\NoSchemaPermission\PermissionGroup;

/**
 * Interface PermissionGroupContract
 * @package Blogator\Components\NoSchemaPermission\Contracts
 */
interface PermissionGroupContract
{
    /**
     * firstly sets permissions to a PermissionGroup and returns it back :)
     * @return PermissionGroup
     */
    public function getPermissionGroup();

    /**
     * json decode permission fields and assign them to Permission and PermissionGroup objects ;)
     * @return $this
     */
    public function setPermissions();

    /**
     * return row id
     * @return mixed
     */
    public function getId();

    /**
     * return row name
     * @return mixed
     */
    public function getName();

}