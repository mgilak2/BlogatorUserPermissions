<?php namespace Blogator\Components\NoSchemaPermission\Contracts;

use Blogator\Components\NoSchemaPermission\Permission;
use Blogator\Components\NoSchemaPermission\PermissionGroup;

interface PermissionStorageContract
{
    public function save();

    public function relateSubGroupsWithThisGroup();
}