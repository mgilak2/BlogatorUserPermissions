<?php namespace Blogator\Components\NoSchemaPermission\Contracts;

interface UserPermissionContract
{
    /**
     * tailored for eloquent
     * @param $name
     * @return bool
     */
    public function hasAccess($name);

    public function getPermission();
}