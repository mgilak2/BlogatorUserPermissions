<?php namespace Blogator\Components\NoSchemaPermission\Exceptions;

use Exception;

/**
 * Class GroupModelNotFoundException
 * @package Blogator\Components\NoSchemaPermission\Exceptions
 */
class GroupModelNotFoundException extends Exception {
}
