<?php namespace Blogator\Components\NoSchemaPermission\Traits;

use Blogator\Components\NoSchemaPermission\Permission;
use Blogator\Components\NoSchemaPermission\PermissionGroup;

/**
 * Class GroupTrait
 * handles PermissionGroupContract functions
 * @package Blogator\Components\NoSchemaPermission\Traits
 */
trait GroupTrait
{
    /**
     * used for singleton pattern
     * @var PermissionGroup
     */
    protected $permissionGroup = null;

    /**
     * firstly sets permissions to a PermissionGroup and returns it back :)
     * @return PermissionGroup
     */
    public function getPermissionGroup()
    {
        if (is_null($this->permissionGroup)) {
            $this->permissionGroup = new PermissionGroup();
            $this->permissionGroup->id = $this->getId();
            $this->permissionGroup->name = $this->getName();
            $this->setPermissions();
        }

        return $this->permissionGroup;
    }

    /**
     * json decode permission fields and assign them to Permission and PermissionGroup objects ;)
     * @return $this
     */
    public function setPermissions()
    {
        $permissionFiledName = $this->getPermission();
        $permissions = json_decode($this->{$permissionFiledName});
        if ($permissions === null) {
            return $this;
        }

        if (isset($permissions->permissions) && is_array($permissions->permissions)) {
            foreach ($permissions->permissions as $permission) {
                $this->permissionGroup->add(new Permission($permission->name, $permission->status));
            }
        }

        return $this;
    }
}