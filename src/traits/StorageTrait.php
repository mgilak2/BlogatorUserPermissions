<?php namespace Blogator\Components\NoSchemaPermission\Traits;

use Blogator\Components\NoSchemaPermission\Permission;
use Blogator\Components\NoSchemaPermission\PermissionGroup;

trait StorageTrait
{
    /**
     * tailored for eloquent
     * @param PermissionGroup $group
     */
    public function save()
    {
    }

    public function relateSubGroupsWithThisGroup()
    {
    }
}