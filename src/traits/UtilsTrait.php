<?php namespace Blogator\Components\NoSchemaPermission\Traits;

trait UtilsTrait
{
    /**
     * @return string
     */
    public function randomString()
    {
        $string = "askj25avfg42pgrojfjbkjbv3jNIJKGTYJr634HKN9KJH2368VTY";
        $id = "";
        foreach (range(1, 12) as $index) {
            $id .= $string[rand(0, 51)];
        }

        return $id;
    }
}