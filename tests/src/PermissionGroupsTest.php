<?php

use Blogator\Components\NoSchemaPermission\Permission;
use Blogator\Components\NoSchemaPermission\PermissionGroup;

class PermissionGroupsTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var PermissionGroup
     */
    protected $group;

    public function setUp()
    {
        $this->group = new PermissionGroup();
    }

    public function add_provider()
    {
        return [
            ['comment-edits', 1],
            ['comment-read', 0],
            ['comment-delete', -1],
        ];
    }

    /**
     * @dataProvider add_provider
     */
    public function test_it_can_add_a_permission_to_a_group($name, $status)
    {
        $permission = new Permission($name, $status);
        $this->group->add($permission);
        $permissions = $this->group->getAllPermissions();
        $this->assertEquals($permissions[$permission->getPermissionForeignId()], $permission);
    }

    /**
     * @dataProvider add_provider
     */
    public function test_it_can_remove_a_permission_in_a_group($name, $status)
    {
        $permission = new Permission($name, $status);
        $this->group->add($permission);
        $permissions = $this->group->getAllPermissions();
        $this->assertEquals($permissions[$permission->getPermissionForeignId()], $permission);
        $this->group->remove($permission);
        $permissions = $this->group->getAllPermissions();
        $this->assertFalse(isset($permissions[$permission->getPermissionForeignId()]));
    }

    public function test_it_can_add_a_group_to_a_group()
    {
        $group1 = new PermissionGroup();
        $this->group->addGroup($group1);
        $groups = $this->group->getAllGroups();
        $this->assertEquals($groups[$group1->getId()], $group1);
    }

    public function test_it_can_remove_a_group_in_a_group()
    {
        $group1 = new PermissionGroup();
        $this->group->addGroup($group1);
        $groups = $this->group->getAllGroups();
        $this->assertEquals($groups[$group1->getId()], $group1);
        $this->group->removeGroup($group1);
        $groups = $this->group->getAllGroups();
        $this->assertFalse(isset($groups[$group1->getId()]));
    }

    public function test_it_can_find_permission_in_permissions_and_subGroups_of_a_permissionGroup_if_its_not_baned()
    {
        $permission0 = new Permission("comment-edit", 1);
        $permission12 = new Permission("comment-delete", -1);
        $this->group->add($permission0);
        $this->group->add($permission12);

        $permission = new Permission("comment-delete", -1);
        $group1 = new PermissionGroup();
        $group1->add(new Permission("comment-edit", 0));
        $group2 = new PermissionGroup();
        $group2->add(new Permission("feed", 1));
        $group3 = new PermissionGroup();
        $group3->add($permission);

        $this->group->addGroup($group1);
        $this->group->addGroup($group2);
        $this->group->addGroup($group3);

        $this->assertTrue($this->group->has(new Permission("feed")));
        $this->assertFalse($this->group->has(new Permission("comment-delete")));
        $this->assertFalse($this->group->has(new Permission("comment-edit")));
    }

    public function tearDown()
    {
        unset($this->group);
    }
}