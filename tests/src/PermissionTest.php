<?php

use Blogator\Components\NoSchemaPermission\Permission;

class PermissionTest extends PHPUnit_Framework_TestCase
{
    public function constructor_provider()
    {
        return [
            ['comment-edits',1,"ok"],
            ['comment-read',0,"noAccess"],
            ['comment-delete',-1,"baned"],
        ];
    }

    /**
     * @dataProvider constructor_provider
     */
    public function test_it_should_set_some_properties_when_we_construct_it($name,$status,$parsed)
    {
        $permission = new Permission($name,$status);
        $this->assertEquals($name,$permission->getName());
        $this->assertEquals($status,$permission->getStatus());
        $this->assertEquals($parsed,$permission->getParsedStatus());
    }
}